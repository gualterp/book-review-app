class BookController <ApplicationController

    # For now... 
    protect_from_forgery with: :null_session

    # Get all books
    def index
        books = Book.all
        render json: BookSerializer.new(books, options).serialized_json
    end

    # Get specific book
    def show 
        book = Book.find_by(name: params[:name])
        render json: BookSerializer.new(book, options).serialized_json
    end

    # Create new book
    def create 
        book = Book.new(book_params)
        if book.save
            render json: BookSerializer.new(book).serialized_json
        else
            render json: {error: book.errors.messages}, status: 400
        end
    end

    # Update book
    def update 
        book = Book.find_by(name: params[:name])
        if book.update(book_params)
            render json: BookSerializer.new(book, options).serialized_json
        else
            render json: {error: book.errors.messages}, status: 400
        end
    end

    # Delete book
    def destroy
        book = Book.find_by(name: params[:name])
        if book.destroy
            head :no_content
        else
            render json: {error: book.errors.messages}, status: 400
        end
    end

    private

    # Define allowed params
    def book_params
        params.require(:book).permit(:name, :image_url)
    end

    # Optional resources
    def options
        @options ||= { include: %i[reviews]}
    end

end