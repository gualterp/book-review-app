class ReviewController <ApplicationController
    
    # For now... 
    protect_from_forgery with: :null_session

    # Add new review
    def create 
        review = Review.new(review_params)
        if review.save
            render json: ReviewSerializer.new(review).serialized_json
        else
            render json: {error: review.errors.messages}, status: 400
        end
    end

    # Delete review
    def destroy 
        review = Review.find(params[:id])
        if review.destroy
            head :no_content
        else
            render json: {error: review.errors.messages}, status: 400
        end
    end

    private

    def review_params
            params.require(:review).permit(:title, :description, :score, :book_id)
    end

end
