class BookSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :image_url, :author

  has_many :reviews
end
