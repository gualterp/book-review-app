class Book < ApplicationRecord
    has_many :reviews

    # Get average review score
    def avg_score
        reviews.average(:score).round(2).to_f
    end
end
