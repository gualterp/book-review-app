# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#


books = Book.create([
    {
        name: "The Melancholy Of Resistance",
        image_url: "https://m.media-amazon.com/images/I/41fTB5LBPCL.jpg",
        author: "László Krasznahorkai"
    },
    {
        name: "Heart Of Darkness",
        image_url: "https://bhaad.me/static/media/images/archive/heart_of_darkness.jpg",
        author: "Joseph Conrad"
    },
    {
        name: "The Trial",
        image_url: "http://2.bp.blogspot.com/-3DYq9vDIquM/Veb72vAALjI/AAAAAAAAFCQ/_7JACQ5Kg20/s320/Cover_Original%2B-%2BThe%2BTrial.png",
        author: "Franz Kafka"
    },
    {
        name: "The Fall",
        image_url: "http://2.bp.blogspot.com/--X5LcGWBIU8/VHezgZLp4gI/AAAAAAAAAl0/gu31vf0yBts/s1600/fall0001.jpg",
        author: "Albert Camus"
    }
])

reviews = Review.create([
    {
        title: "Good book",
        description: "What a good book.",
        score: 5,
        book: books.first
    },
    {
        title: "Bad book",
        description: "What a bad book.",
        score: 2,
        book: books.first
    }
])
