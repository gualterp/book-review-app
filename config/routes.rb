Rails.application.routes.draw do  
  root 'pages#index'  
  resources :book, param: :name
  resources :review, only: [:create, :destroy]

  get '*path', to: 'pages#index', via: :all

end
